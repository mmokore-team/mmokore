#=============================================================
#                          Clans config
#=============================================================

clans {

  other {
    # Number of days you have to wait before joining another clan
    daysBeforeJoinAClan = 1

    # Number of days you have to wait before creating a new clan
    daysBeforeCreateAClan = 10

    # Number of days it takes to dissolve a clan
    daysToPassToDissolveAClan = 7

    # Number of days before joining a new alliance when clan voluntarily leave an alliance
    daysBeforeJoinAllyWhenLeaved = 1

    # Number of days before joining a new alliance when clan was dismissed from an alliance
    daysBeforeJoinAllyWhenDismissed = 1

    # Number of days before accepting a new clan for alliance when clan was dismissed from an alliance
    daysBeforeAcceptNewClanWhenDismissed = 1

    # Number of days before creating a new alliance when dissolved an alliance
    daysBeforeCreateNewAllyWhenDissolved = 10

    # Maximum number of clans in ally
    altMaxNumOfClansInAlly = 3

    # Number of members needed to request a clan war
    altClanMembersForWar = 15

    # Number of days needed by a clan to war back another clan
    altClanWarPenaltyWhenEnded = 5

    # Privilege browse warehouse enables at the same time also withdraw from warehouse!
    altMembersCanWithdrawFromClanWH = false

    #Remove Castle circlets after a clan lose its castle or a player leaves a clan? - default true
    removeCastleCirclets = true
  }

  #=============================================================
  #                         Manor Config
  #=============================================================
  manor {
    refreshTime = 20 # Manor refresh time in hours. Default 8pm (20).
    refreshMin = 0 # Manor refresh time for minutes. Default 0 (start of the hour).
    approveTime = 6 # Manor next period approve time in hours. Default 6am.
    approveMin = 0 # Manor next period approve time for minutes. Default 0 (start of the hour).
    maintenanceMin = 6 # Manor maintenance time. Default 6 minutes.
    savePeriodRate = 2 # Manor save period. Default every 2 hours.
  }

  #=============================================================
  #                   Clan Hall Functions
  #=============================================================
  clanHall {

    #-------------------------------------------------------------
    # Teleport Function price
    #-------------------------------------------------------------
    teleportFee {
      level1 = 7000
      level2 = 14000
    }

    #-------------------------------------------------------------
    # Support magic buf price
    #-------------------------------------------------------------
    supportBuffsFee {
      level1 = 17500
      level2 = 35000
      level3 = 49000
      level4 = 77000
      level5 = 147000
      level6 = 252000
      level7 = 259000
      level8 = 364000
    }

    regeneration {

      #-------------------------------------------------------------
      #  HpRegeneration price
      #-------------------------------------------------------------
      hpFee {
        level1 = 4900 # 20% HpRegeneration
        level2 = 5600 # 40% HpRegeneration
        level3 = 7000 # 80% HpRegeneration
        level4 = 8166 # 100% HpRegeneration
        level5 = 10500 # 120% HpRegeneration
        level6 = 12250 # 140% HpRegeneration
        level7 = 14000 # 160% HpRegeneration
        level8 = 15750 # 180% HpRegeneration
        level9 = 17500 # 200% HpRegeneration
        level10 = 22750 # 220% HpRegeneration
        level11 = 26250 # 240% HpRegeneration
        level12 = 29750 # 260% HpRegeneration
        level13 = 36166 # 300% HpRegeneration
      }

      #-------------------------------------------------------------
      #  MpRegeneration price
      #-------------------------------------------------------------
      mpFee {
        level1 = 14000 # 5% MpRegeneration
        level2 = 26250 # 10% MpRegeneration
        level3 = 45500 # 15% MpRegeneration
        level4 = 96250 # 30% MpRegeneration
        level5 = 140000 # 40% MpRegeneration
      }

      #-------------------------------------------------------------
      #  ExpRegeneration
      #-------------------------------------------------------------
      experienceFee {
        level1 = 21000 # 5% ExpRegeneration
        level2 = 42000 # 10% ExpRegeneration
        level3 = 63000 # 15% ExpRegeneration
        level4 = 105000 # 25% ExpRegeneration
        level5 = 147000 # 35% ExpRegeneration
        level6 = 163331 # 40% ExpRegeneration
        level7 = 210000 # 50% ExpRegeneration
      }
    }

    #-------------------------------------------------------------
    # Creation item function
    #-------------------------------------------------------------
    creationItems {
      #Time after count of Item is restored (in Hours)
      taskRestore = 24

      creationFee {
        level1 = 210000
        level2 = 490000
        level3 = 980000
      }
    }

    #-------------------------------------------------------------
    # Decor function
    #-------------------------------------------------------------
    # Need core support, need more information on functions in different
    # Clan Hall in different Towns.
    decoration {

      curtainFee {
        level1 = 2002
        level2 = 2625
      }

      frontPlatformFee {
        level1 = 3031
        level2 = 9331
      }
    }
  }
}
