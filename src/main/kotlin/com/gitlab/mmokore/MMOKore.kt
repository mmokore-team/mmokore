package com.gitlab.mmokore

import com.gitlab.mmokore.mods.ModsManager

class MMOKore {

    init {
        println("Initializing ModsManager")
        ModsManager.initialize()
    }
}
