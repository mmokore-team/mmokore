package com.gitlab.mmokore.engines.watchers

import com.gitlab.mmokore.engines.watchers.enums.ActionType
import com.gitlab.mmokore.engines.watchers.interfaces.IAction
import com.gitlab.mmokore.engines.watchers.interfaces.IActionWatcher
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

object ActionsDispatcher {

    private val subscribers: MutableMap<ActionType, MutableSet<IActionWatcher>> = HashMap()

    fun addSuscription(watcher: IActionWatcher) {
        watcher.getTypes().forEach { type ->
            synchronized(subscribers) {
                subscribers.putIfAbsent(type, LinkedHashSet())
                subscribers[type]?.add(watcher)
                subscribers[type]?.sortedBy { watcher -> watcher.priority() }
            }
        }
    }

    fun removeWatcher(watcher: IActionWatcher) {
        synchronized(subscribers) {
            subscribers.values.forEach { watchers -> watchers.remove(watcher) }
        }
    }

    fun dispatch(action: IAction) {
        subscribers[action.getType()]?.forEach { watcher -> if (!action.aborted()) watcher.onReceive(action) }
    }

    fun dispatchAsync(action: IAction) {
        subscribers[action.getType()]?.forEach { watcher ->
            GlobalScope.launch {
                watcher.onReceive(action)
            }
        }
    }
}
