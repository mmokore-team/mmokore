package com.gitlab.mmokore.engines.watchers.interfaces

import com.gitlab.mmokore.engines.watchers.enums.WatcherPriority
import com.gitlab.mmokore.engines.watchers.enums.ActionType

interface IActionWatcher {

    fun priority(): WatcherPriority
    fun getTypes(): Collection<ActionType>
    fun onReceive(action: IAction)
}
