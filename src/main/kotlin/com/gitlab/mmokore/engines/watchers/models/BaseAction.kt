package com.gitlab.mmokore.engines.watchers.models

import com.gitlab.mmokore.engines.watchers.interfaces.IAction

abstract class BaseAction : IAction {

    private var aborted = false

    override fun aborted() = aborted
    override fun abort() { aborted = true }

}
