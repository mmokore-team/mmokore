package com.gitlab.mmokore.engines.watchers.enums

enum class WatcherPriority(val order : Int) {
    CRITICAL(0),
    HIGHEST(1),
    HIGH(2),
    NORMAL(3),
    LOW(4),
    LOWEST(5)
}
