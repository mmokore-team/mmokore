package com.gitlab.mmokore.engines.watchers.interfaces

import com.gitlab.mmokore.engines.watchers.enums.ActionType

interface IAction {    fun getType() : ActionType
    fun aborted() : Boolean
    fun abort()
}
