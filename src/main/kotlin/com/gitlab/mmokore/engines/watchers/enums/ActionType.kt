package com.gitlab.mmokore.engines.watchers.enums

enum class ActionType {

    ON_ADV,
    ON_AGGRO_ENTER,
    ON_ATTACK,
    ON_ATTACK_BY_NPC,
    ON_DEATH,
    ON_DECAY,
    ON_ENTER_WORLD,
    ON_ENTER_ZONE,
    ON_EXIT_ZONE,
    ON_FACTION_CALL,
    ON_FIRST_TALK,
    ON_ITEM_USE,
    ON_KILL,
    ON_SIEGE,
    ON_SKILL_SEE,
    ON_SPAWN,
    ON_SPELL_FINISHED,
    ON_TALK

}
