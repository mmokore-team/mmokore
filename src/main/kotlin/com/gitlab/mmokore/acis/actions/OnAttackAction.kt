package com.gitlab.mmokore.acis.actions

import com.gitlab.mmokore.engines.watchers.enums.ActionType
import com.gitlab.mmokore.engines.watchers.models.BaseAction
import net.sf.l2j.gameserver.model.L2Skill
import net.sf.l2j.gameserver.model.actor.Npc
import net.sf.l2j.gameserver.model.actor.instance.Player

/**
 * Quest event notifycator for player's or player's pet attack.
 * @param npc Attacked npc instace.
 * @param attacker Attacker or pet owner.
 * @param damage Given damage.
 * @param isPet Player summon attacked?
 * @param skill the skill used to attack the NPC (can be null)
 */
class OnAttackAction(val npc : Npc, val attacker : Player,
                     val damage : Integer, val isPet : Boolean,
                     val skill : L2Skill) : BaseAction() {

    override fun getType() = ActionType.ON_ATTACK
}
