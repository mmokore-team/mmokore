package com.gitlab.mmokore.acis.actions

import com.gitlab.mmokore.engines.watchers.enums.ActionType
import com.gitlab.mmokore.engines.watchers.models.BaseAction
import net.sf.l2j.gameserver.model.WorldObject
import net.sf.l2j.gameserver.model.actor.instance.Player
import net.sf.l2j.gameserver.model.item.instance.ItemInstance

class OnItemUseAction(val item : ItemInstance, val player : Player, val target : WorldObject) : BaseAction() {
    override fun getType() = ActionType.ON_ITEM_USE
}
