package com.gitlab.mmokore.acis.actions

import com.gitlab.mmokore.engines.watchers.enums.ActionType
import com.gitlab.mmokore.engines.watchers.models.BaseAction

class OnSiegeAction(val castleId : Integer) : BaseAction() {
    override fun getType() = ActionType.ON_SIEGE
}
