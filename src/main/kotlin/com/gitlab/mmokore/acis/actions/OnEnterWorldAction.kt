package com.gitlab.mmokore.acis.actions

import com.gitlab.mmokore.engines.watchers.enums.ActionType
import com.gitlab.mmokore.engines.watchers.models.BaseAction
import net.sf.l2j.gameserver.model.actor.instance.Player

class OnEnterWorldAction(val player : Player) : BaseAction() {
    override fun getType() = ActionType.ON_ENTER_WORLD
}
