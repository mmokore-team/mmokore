package com.gitlab.mmokore.acis.actions

import com.gitlab.mmokore.engines.watchers.enums.ActionType
import com.gitlab.mmokore.engines.watchers.models.BaseAction
import net.sf.l2j.gameserver.model.L2Skill
import net.sf.l2j.gameserver.model.WorldObject
import net.sf.l2j.gameserver.model.actor.Npc
import net.sf.l2j.gameserver.model.actor.instance.Player

class OnSkillSeeAction(val npc : Npc, val caster : Player, val skill : L2Skill,
                       val targets : List<WorldObject>, val isPet : Boolean) : BaseAction() {
    override fun getType() = ActionType.ON_SKILL_SEE
}
