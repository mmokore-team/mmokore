package com.gitlab.mmokore.acis.actions

import com.gitlab.mmokore.engines.watchers.enums.ActionType
import com.gitlab.mmokore.engines.watchers.models.BaseAction
import net.sf.l2j.gameserver.model.actor.Npc
import net.sf.l2j.gameserver.model.actor.instance.Player

class OnKillAction(val npc : Npc, val killer : Player, val isPet : Boolean) : BaseAction() {
    override fun getType() = ActionType.ON_KILL
}
