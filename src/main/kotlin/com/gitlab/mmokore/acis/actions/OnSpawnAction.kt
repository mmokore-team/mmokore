package com.gitlab.mmokore.acis.actions

import com.gitlab.mmokore.engines.watchers.enums.ActionType
import com.gitlab.mmokore.engines.watchers.models.BaseAction
import net.sf.l2j.gameserver.model.actor.Npc

class OnSpawnAction(val npc : Npc) : BaseAction() {
    override fun getType() = ActionType.ON_SPAWN
}
