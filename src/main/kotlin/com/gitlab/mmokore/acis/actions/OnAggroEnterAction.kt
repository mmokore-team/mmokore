package com.gitlab.mmokore.acis.actions

import com.gitlab.mmokore.engines.watchers.enums.ActionType
import com.gitlab.mmokore.engines.watchers.models.BaseAction
import net.sf.l2j.gameserver.model.actor.Npc
import net.sf.l2j.gameserver.model.actor.instance.Player

class OnAggroEnterAction(val npc : Npc, val player : Player, val isPet : Boolean) : BaseAction() {
    override fun getType() = ActionType.ON_AGGRO_ENTER
}
