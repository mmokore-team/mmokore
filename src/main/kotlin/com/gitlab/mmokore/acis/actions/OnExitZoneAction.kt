package com.gitlab.mmokore.acis.actions

import com.gitlab.mmokore.engines.watchers.enums.ActionType
import com.gitlab.mmokore.engines.watchers.models.BaseAction
import net.sf.l2j.gameserver.model.actor.Creature
import net.sf.l2j.gameserver.model.zone.L2ZoneType

class OnExitZoneAction(val character : Creature, val zone : L2ZoneType) : BaseAction() {
    override fun getType() = ActionType.ON_EXIT_ZONE
}
