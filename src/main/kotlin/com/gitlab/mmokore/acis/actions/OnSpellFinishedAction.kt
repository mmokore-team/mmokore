package com.gitlab.mmokore.acis.actions

import com.gitlab.mmokore.engines.watchers.enums.ActionType
import com.gitlab.mmokore.engines.watchers.models.BaseAction
import net.sf.l2j.gameserver.model.L2Skill
import net.sf.l2j.gameserver.model.actor.Npc
import net.sf.l2j.gameserver.model.actor.instance.Player

class OnSpellFinishedAction(val npc : Npc, val player : Player, val skill : L2Skill) : BaseAction() {
    override fun getType() = ActionType.ON_SPELL_FINISHED
}
