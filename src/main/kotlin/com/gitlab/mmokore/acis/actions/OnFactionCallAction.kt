package com.gitlab.mmokore.acis.actions

import com.gitlab.mmokore.engines.watchers.enums.ActionType
import com.gitlab.mmokore.engines.watchers.models.BaseAction
import net.sf.l2j.gameserver.model.actor.Npc
import net.sf.l2j.gameserver.model.actor.instance.Player

class OnFactionCallAction(val npc : Npc, val caller : Npc, val attacker : Player,
                          val isPet : Boolean) : BaseAction() {
    override fun getType() = ActionType.ON_FACTION_CALL
}
