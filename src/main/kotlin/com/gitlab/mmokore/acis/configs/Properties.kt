package com.gitlab.mmokore.acis.configs

import com.gitlab.mmokore.acis.configs.ClansConfig
import com.gitlab.mmokore.acis.configs.LoginServerConfig
import com.gitlab.mmokore.acis.configs.SiegeConfig

object Properties {

    val siege = SiegeConfig("siege.conf")
    val clans = ClansConfig("clans.conf")
    val loginServer = LoginServerConfig("loginserver.conf")

}
