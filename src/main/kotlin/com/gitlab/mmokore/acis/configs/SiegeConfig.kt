package com.gitlab.mmokore.acis.configs

import com.uchuhimo.konf.ConfigSpec

class SiegeConfig(file: String) : PropertyConfig(file) {

    override fun getConfig() = Siege

    protected object Siege : ConfigSpec() {
        val times by required<Times>()
        val clans by required<Clans>()
    }

    val times = config[Siege.times]
    val clans = config[Siege.clans]

    class Times {
        val length = -1
        val attackerRespawn = -1
    }

    class Clans {
        val minLevel = -1
        val attackerMaxClans = -1
        val defenderMaxClans = -1
    }
}

