package com.gitlab.mmokore.acis.configs

import com.uchuhimo.konf.ConfigSpec

class EventsConfig(file: String) : PropertyConfig(file) {

    override fun getConfig() = Events

    protected object Events : ConfigSpec() {
        val olympiad by required<Olympiad>()
        val sevenSigns by required<SevenSigns>()
        val fourSepulchers by required<FourSepulchers>()
        val dimensionalRift by required<DimensionalRift>()
        val wedding by required<Wedding>()
        val lottery by required<Lottery>()
        val fishChampionship by required<FishChampionship>()
    }

    val olympiad = config[Events.olympiad]
    val sevenSigns = config[Events.sevenSigns]
    val fourSepulchers = config[Events.fourSepulchers]
    val dimensionalRift = config[Events.dimensionalRift]
    val wedding = config[Events.wedding]
    val lottery = config[Events.lottery]
    val fishChampionship = config[Events.fishChampionship]

    class Olympiad {
        val startTimeHour = 18
        val startTimeMinutes = 0
        val period = 21600000
        val battleLength = 360000
        val weeklyPeriod = 604800000
        val validationPeriod = 86400000
        val waitTime = 30
        val waitBattleTime = 60
        val waitEndTime = 40
        val startPoints = 18
        val weeklyPoints = 3
        val minMatchesToBeClassed = 9
        val minParticipantsInClassedGames = 5
        val minParticipantsInNonClassedGames = 9
        val classedRewards = listOf<Reward>(Reward(6651, 50))
        val nonClassedRewards = listOf<Reward>(Reward(6651, 30))
        val gpPerPoint = 1000
        val heroPoints = 300
        val rankPoints = RankPoints()
        val maxPointsToGainOrLose = 10
        val announcePerFightGames = true
        val dividerClassed = 3
        val dividerNonClassed = 5

        class Reward(val itemId : Int, val itemAmount : Int)

        class RankPoints {
            val rank1 = 100
            val rank2 = 75
            val rank3 = 55
            val rank4 = 40
            val rank5 = 30
        }
    }

    class SevenSigns {
        val castleForDawn = true
        val castleForDusk = true

        class Festival {
            val minPlayers = 5
            val maxContributionPerPlayer = 1000000

            class Times {
                val start = 120000
                val length = 1080000
                val cycleLength = 2280000
                val firstSpawn = 120000
                val firstSwarm = 300000
                val secondSpawn = 540000
                val secondSwarm = 720000
                val chestsSpawn = 900000
            }
        }
    }

    class FourSepulchers {
        val minPartyMembers = 4
        val times = Times()

        class Times {
            val attack = 50
            val entry = 3
            val warmUp = 2
        }
    }

    class DimensionalRift {
        val minPartySize = 2
        val maxRiftJumps = 4
        val spawnDelay = 10000
        val autoJumpsDelayMin = 480
        val autoJumpsDelayMax = 600
        val bossRoomTimeMultiply = 1.0
        val costs = Costs()

        class Costs {
            val recruit = 18
            val soldier = 21
            val officer = 24
            val captain = 27
            val commander = 30
            val hero = 33
        }
    }

    class Wedding {
        val enabled = false
        val price = 1000000
        val allowSameSex = false
        val formalWear = true
    }

    class Lottery {
        val initialPrize = 50000
        val ticketPrize = 2000
        val rateNumber5 = 0.6
        val rateNumber4 = 0.2
        val rateNumber3 = 0.2
        val rateNumber2And1 = 200
    }

    class FishChampionship {

        val enabled = true
        val rewards = Rewards()

        class Rewards {
            val itemId = 57
            val amountTop1 = 800000
            val amountTop2 = 500000
            val amountTop3 = 300000
            val amountTop4 = 200000
            val amountTop5 = 100000
        }
    }
}
