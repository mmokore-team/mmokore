package com.gitlab.mmokore.acis.configs

import com.uchuhimo.konf.Config
import com.uchuhimo.konf.ConfigSpec

abstract class PropertyConfig(file: String) {

    protected val config = Config { addSpec(getConfig()) }
        .from.hocon.resource(file)

    protected abstract fun getConfig(): ConfigSpec
}
