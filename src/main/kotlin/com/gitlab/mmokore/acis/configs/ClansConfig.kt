package com.gitlab.mmokore.acis.configs

import com.uchuhimo.konf.ConfigSpec

class ClansConfig(file: String) : PropertyConfig(file) {

    override fun getConfig() = Clans

    protected object Clans : ConfigSpec() {
        val manor by required<Manor>()
        val clanHall by required<ClanHall>()
        val other by required<Other>()
    }

    val manor = config[Clans.manor]
    val clanHall = config[Clans.clanHall]
    val other = config[Clans.other]

    class Manor {
        val refreshTime = -1
        val refreshMin = -1
        val approveTime = -1
        val approveMin = -1
        val maintenanceMin = -1
        val savePeriodRate = -1
    }

    class ClanHall {

        val teleportFee = TeleportFee()
        val supportBuffsFee = SupportBuffsFee()
        val regeneration = Regeneration()
        val creationItems = CreationItems()
        val decoration = Decoration()

        class TeleportFee {
            val level1 = 7000
            val level2 = 14000
        }

        class SupportBuffsFee {
            val level1 = 17500
            val level2 = 35000
            val level3 = 49000
            val level4 = 77000
            val level5 = 147000
            val level6 = 252000
            val level7 = 259000
            val level8 = 364000
        }

        class Regeneration {

            val hpFee = HpFee()
            val mpFee = MpFee()
            val experienceFee = ExperienceFee()

            class HpFee {
                val level1 = 4900
                val level2 = 5600
                val level3 = 7000
                val level4 = 8166
                val level5 = 10500
                val level6 = 12250
                val level7 = 14000
                val level8 = 15750
                val level9 = 17500
                val level10 = 22750
                val level11 = 26250
                val level12 = 29750
                val level13 = 36166
            }

            class MpFee {
                val level1 = 14000
                val level2 = 26250
                val level3 = 45500
                val level4 = 96250
                val level5 = 140000
            }

            class ExperienceFee {
                val level1 = 21000
                val level2 = 42000
                val level3 = 63000
                val level4 = 105000
                val level5 = 147000
                val level6 = 163331
                val level7 = 210000
            }
        }

        class CreationItems {

            val taskRestore = 24
            val creationFee = CreationFee()

            class CreationFee {
                val level1 = 210000
                val level2 = 490000
                val level3 = 980000
            }
        }

        class Decoration {

            val curtainFee = CurtainFee()
            val frontPlatformFee = FrontPlatformFee()

            class CurtainFee {
                val level1 = 2002
                val level2 = 2625
            }

            class FrontPlatformFee {
                val level1 = 3031
                val level2 = 9331
            }
        }
    }

    class Other {
        val daysBeforeJoinAClan = -1
        val daysBeforeCreateAClan = -1
        val daysToPassToDissolveAClan = -1
        val daysBeforeJoinAllyWhenLeaved = -1
        val daysBeforeJoinAllyWhenDismissed = -1
        val daysBeforeAcceptNewClanWhenDismissed = -1
        val daysBeforeCreateNewAllyWhenDissolved = -1
        val altMaxNumOfClansInAlly = -1
        val altClanMembersForWar = -1
        val altClanWarPenaltyWhenEnded = -1
        val altMembersCanWithdrawFromClanWH = false
        val removeCastleCirclets = true
    }
}
