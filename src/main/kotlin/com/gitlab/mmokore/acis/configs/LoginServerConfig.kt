package com.gitlab.mmokore.acis.configs

import com.uchuhimo.konf.ConfigSpec

class LoginServerConfig(file: String) : PropertyConfig(file) {

    override fun getConfig() = LoginServer

    object LoginServer : ConfigSpec() {
        val general by required<General>()
        val database by required<Database>()
        val security by required<Security>()
        val test by required<Test>()
    }

    val general = config[LoginServer.general]
    val database = config[LoginServer.database]
    val security = config[LoginServer.security]
    val test = config[LoginServer.test]

    class General {
        val hostname = "localhost"
        val serverHostname = "*"
        val serverPort = 2106
        val loginHostname = "*"
        val port = 9014
        val tryBeforeBan = 3
        val blockAfterBan = 600
        val acceptNewGameServer = false
        val showLicence = true
    }

    class Database {
        val url = "jdbc:mysql://localhost/acis"
        val login = ""
        val password = ""
        val maximumDbConnections = 10
        val autoCreateAccounts = true
    }

    class Security {
        val logLoginController = false
        val floodProtection = true
        val fastConnectionLimit = 15
        val normalConnectionTime = 700
        val fastConnectionTime = 350
        val maxConnectionPerIP = 50
    }

    class Test {
        val debug = false
        val developer = false
        val packetHandlerDebug = false
    }
}
